#!/usr/bin/python
#-*- coding:utf-8 -*-
'''
Created on 2013-11-12

@author: xmxjq
'''
from bs4 import BeautifulSoup
import json
import urllib
import urllib2
import time

import sys
reload(sys)
if hasattr(sys, "setdefaultencoding"):
    sys.setdefaultencoding("utf-8")

def request_ajax_url(url,url_to_send,referer=None,cookie=None,**headers):
    req = urllib2.Request(url)
    req.add_header('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
    req.add_header('X-Requested-With','XMLHttpRequest')

    if cookie:
        req.add_header('Cookie',cookie)
    if referer:
        req.add_header('Referer',referer)
    if headers:
        for k in headers.keys():
            req.add_header(k,headers[k])
 
    postBody = urllib.urlencode([('url', url_to_send)])
#    print postBody
    response = urllib2.urlopen(req, postBody)
    if response:
        return response
    
def send_url_to_server(url):
    server_url = 'http://202.120.36.179:20080/ajax/submit_url'
    referer = "http://202.120.36.179:20080/"
    response = request_ajax_url(server_url, url, referer)
#    print response
    
def table_has_class_test(tag):
    if tag.name == 'table':
#        print tag
        if tag.has_attr('class'):
            return u'test' in tag['class']
        else:
            return False
    else:
        return False
    
def report_page_process(href):
#    print 'http://urlquery.net/' + href
    response = urllib2.urlopen('http://urlquery.net/' + href)
    html = response.read()
    soup = BeautifulSoup(html)
    overview_table = soup.find('h2').find_next_sibling('table')
#    for child in overview_table.tbody.children:
#        print(child)
    alert = str(overview_table.tbody.find_all('tr')[6].find_all('td')[1])
    print alert
    if 'malicious' in alert or 'CookieBomb' in alert:
        return True
    else:
        return False

if __name__ == '__main__':    
    url_list = list()
    response = urllib2.urlopen('http://urlquery.net/index.php')
    html = response.read()
    soup = BeautifulSoup(html)
#    print html
    main_table = soup.findAll(table_has_class_test).pop()
    print time.strftime('%Y-%m-%d %A %X\n',time.localtime(time.time()))
    for child in main_table.tbody.children:
        if child.name == u'tr':
            if not child.contents[1].string.startswith('0'):
#                print child.contents[1].string
                print child.contents[2].a['title']
#                if report_page_process(child.contents[2].a['href']):
                url_list.append(unicode(child.contents[2].a['title']))
#    print url_list

#    for url in url_list:
#        print "Sending: " + url
#        send_url_to_server(url)
